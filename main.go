package apilib

var messages = map[int]string{
	100: "success",
	110: "internal_error",
	111: "database_error",
	120: "bad_request",
	130: "failed_to_authenticate",
	131: "invalid_credentials",
	140: "processing_error",
	150: "no_valid_data",
}

const(
	MessageSuccess = 100
	MessageInternalError = 110
	MessageDatabaseError = 111
	MessageBadRequest = 120
	MessageFailedToAuthenticate = 130
	MessageInvalidCredentials = 131
	MessageProcessError = 140
	MessageNoValidData = 150
)

func CreateResponse(code int) Response {
	status, ok := messages[code]
	if !ok {
		return Response{}
	}
	r := Response{
		Status:  status,
		Code:    code,
		Message: "",
		Data:    nil,
	}
	return r
}
